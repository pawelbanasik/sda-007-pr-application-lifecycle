package com.example.rent.sda_007_pr_application_lifecycle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edit_text_one)
    protected EditText editTextOne;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
    @OnClick (R.id.button_one)
    public void clickMethodOne(View v) {

        // utworzenie intentu
        Intent intent = new Intent(getApplicationContext(), Main2Activity.class);

        // dodanie dodatkowego parametru
        intent.putExtra("Parametr", editTextOne.getText().toString());

        // uruchomienie activity z intentu
        startActivity(intent);
    }
}
