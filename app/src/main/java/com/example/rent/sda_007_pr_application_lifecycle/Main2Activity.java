package com.example.rent.sda_007_pr_application_lifecycle;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main2Activity extends AppCompatActivity {

    @BindView(R.id.text_view_one)
    protected TextView textViewOne;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("Parametr")) {
            textViewOne.setText(getIntent().getStringExtra("Parametr"));
        }
    }


}
